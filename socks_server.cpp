#include <iostream>
#include <fstream>
#include <unistd.h>
#include <signal.h>

#include "tcp_socket.hpp"

#define BUF_SIZE 60000

using namespace std;

int child_count;

/* signal handler : decrease user count while child process exits */
void child_exit(int signum) {
    if (signum == SIGCHLD) {
        child_count--;
    }
}

/* exit(0) if read eof from from_fd */
void transport(int from_fd, int to_fd) {
    char buffer[BUF_SIZE] = {0};
    int read_count = read(from_fd, buffer, BUF_SIZE);
    if (read_count == 0) { /* read() eof */
        exit(0);
    } else if (read_count == -1) {
        cerr_exit(error_ss << "Read error");
    } else {
        int write_count = write(to_fd, buffer, read_count);
        if (write_count != read_count) {
            cout << "[Warning] from_fd -> to_fd:  write_count != read_count" << endl;
        }
    }
}

void socks4_handler(int client_fd, const sockaddr_in client_addr) {
    u_char request[BUF_SIZE], reply[8] = {0};
    /* read socks4 request */
    int read_count = read(client_fd, request, BUF_SIZE);
    switch (read_count) {
        case -1:
            cerr_exit(error_ss << "Client connection fail");
        case 0:
            cout << "Client disconnect" << endl;
            exit(0);
        default:
            break;
    }
    /* parse requset */
    u_char sp_vn = request[0];
    u_char sp_cd = request[1];
    u_int sp_dst_port = request[2] << 8 | request[3];
    u_int sp_dst_ip = request[7] << 24 | request[6] << 16 | request[5] << 8 | request[4];
    char *sp_user_id = (char *)request + 8;
    if (sp_vn != 4) {
        cerr_exit(error_ss << "Bad sock4 request, VN = " << sp_vn);
    }
    /* sock4a support */
    char sp_dst_ip_cstr[INET_ADDRSTRLEN];
    if (request[4] == 0 && request[5] == 0 && request[6] == 0 && request[7] != 0) {
        /* sock4a */
        size_t user_id_len = strlen(sp_user_id);
        char *sp_domain_name = sp_user_id + user_id_len + 1;
        /* resolve ip address by domain_name */
        struct hostent *phe;
        if (phe = gethostbyname(sp_domain_name)) {
            struct sockaddr_in sin;
            bcopy(phe->h_addr, (char *)&sin.sin_addr, phe->h_length);
            inet_ntop(AF_INET, &sin.sin_addr, sp_dst_ip_cstr, INET_ADDRSTRLEN);
        } else {
            cerr_exit(error_ss << "Get ip by domain_name failed, domain_name = " << sp_domain_name);
        }
    } else {
        /* sock4 */
        inet_ntop(AF_INET, &sp_dst_ip, sp_dst_ip_cstr, INET_ADDRSTRLEN);
    }
    /* check with firewall */
    ifstream fin("socks.conf");
    if (fin) {
        string rule, mode, addr_str, addr_part[4];
        reply[1] = 91; /* default with rejected */
        while (fin >> rule >> mode >> addr_str) {
            for (int i = 0; i < 4; i++) {
                auto pos = addr_str.find_first_of('.');
                if (pos != string::npos) {
                    addr_part[i] = addr_str.substr(0, pos);
                    addr_str.erase(0, pos + 1);
                } else {
                    addr_part[i] = addr_str;
                }
            }
            if (((mode == "c" && sp_cd == 1) || (mode == "b" && sp_cd == 2)) &&
                (addr_part[0] == "*" || ((u_char)stoul(addr_part[0]) == request[4])) &&
                (addr_part[1] == "*" || ((u_char)stoul(addr_part[1]) == request[5])) &&
                (addr_part[2] == "*" || ((u_char)stoul(addr_part[2]) == request[6])) &&
                (addr_part[3] == "*" || ((u_char)stoul(addr_part[3]) == request[7]))) {
                reply[1] = 90;
                break;
            }
        }
    } else {
        cout << "[Warning] Can not open \"socks.conf\", all accept" << endl;
        reply[1] = 90;
    }
    /* show socks request info */
    char s_ip_cstr[INET_ADDRSTRLEN];
    int s_port = ntohs(client_addr.sin_port);
    inet_ntop(AF_INET, &client_addr.sin_addr, s_ip_cstr, INET_ADDRSTRLEN);
    cout << "<S_IP>:    " << s_ip_cstr << endl
         << "<S_PORT>:  " << s_port << endl
         << "<D_IP>:    " << sp_dst_ip_cstr << endl
         << "<D_PORT>:  " << sp_dst_port << endl
         << "<Command>: " << ((sp_cd == 1) ? "CONNECT" : "BIND") << endl
         << "<Reply>:   " << ((reply[1] == 90) ? "Accept" : "Reject") << endl
         << endl;
    /* reply reject */
    if (reply[1] == 91) {
        for (int i = 2; i < 8; i++) {
            reply[i] = request[i];
        }
        write(client_fd, reply, 8);
        exit(0);
    }
    /* 1: connect mode, 2: bind mode */
    fd_set read_fdset, act_fdset;
    FD_ZERO(&act_fdset);
    if (sp_cd == 1) {
        /* reply accept */
        for (int i = 2; i < 8; i++) {
            reply[i] = request[i];
        }
        write(client_fd, reply, 8);
        int target_fd = client_sock(sp_dst_ip_cstr, sp_dst_port);
        /* transport data between socks client & target server */
        int maxfd_num = max(client_fd, target_fd) + 1;
        FD_SET(target_fd, &act_fdset);
        FD_SET(client_fd, &act_fdset);
        while (true) {
            memcpy(&read_fdset, &act_fdset, sizeof(read_fdset));
            if (select(maxfd_num, &read_fdset, NULL, NULL, NULL) > 0) {
                if (FD_ISSET(client_fd, &read_fdset)) {
                    transport(client_fd, target_fd);
                }
                if (FD_ISSET(target_fd, &read_fdset)) {
                    transport(target_fd, client_fd);
                }
            }
        }
    } else if (sp_cd == 2) {
        int bind_fd = server_sock(0, 30); /* port = 0 (auto assign port) */
        /* get info of bind_fd */
        struct sockaddr_in bind_addr;
        u_int bind_addr_len = sizeof(bind_addr);
        if (getsockname(bind_fd, (struct sockaddr *)&bind_addr, &bind_addr_len) == -1) {
            cerr_exit(error_ss << "Can't get sockaddr_in of bind_fd");
        }
        /* reply accept */
        reply[2] = (u_char)(ntohs(bind_addr.sin_port) / 256);
        reply[3] = (u_char)(ntohs(bind_addr.sin_port) % 256);
        for (int i = 4; i < 8; ++i) {
            reply[i] = 0; /* 0.0.0.0 means the same ip to socks server */
        }
        write(client_fd, reply, 8);
        /* connected from ftp server */
        struct sockaddr_in ftp_addr;
        u_int ftp_addr_len = sizeof(ftp_addr);
        int ftp_fd = accept(bind_fd, (struct sockaddr *)&ftp_addr, &ftp_addr_len);
        if (ftp_fd < 0) {
            cerr_exit(error_ss << "Accept error : ftp_fd");
        }
        /* check the ip from the ftp server */
        u_int except_ip;
        inet_pton(AF_INET, sp_dst_ip_cstr, &except_ip);
        if (ftp_addr.sin_addr.s_addr != except_ip) {
            reply[1] = 91;
            write(client_fd, reply, 8); /* reply reject */
            char ftp_ip_cstr[INET_ADDRSTRLEN];
            inet_ntop(AF_INET, &ftp_addr.sin_addr, ftp_ip_cstr, INET_ADDRSTRLEN);
            cerr_exit(error_ss << "Bad incoming connection, ip = " << ftp_ip_cstr);
        }
        /* reply again after being connected from ftp server */
        write(client_fd, reply, 8);
        /* transport data between socks client & ftp server */
        int maxfd_num = max(client_fd, ftp_fd) + 1;
        FD_SET(ftp_fd, &act_fdset);
        FD_SET(client_fd, &act_fdset);
        while (true) {
            memcpy(&read_fdset, &act_fdset, sizeof(read_fdset));
            if (select(maxfd_num, &read_fdset, NULL, NULL, NULL) > 0) {
                if (FD_ISSET(client_fd, &read_fdset)) {
                    transport(client_fd, ftp_fd);
                }
                if (FD_ISSET(ftp_fd, &read_fdset)) {
                    transport(ftp_fd, client_fd);
                }
            }
        }
    } else {
        cerr_exit(error_ss << "Bad sock4 request, CD = " << (uint)sp_cd);
    }
}

int main(int argc, char const *argv[]) {
    struct sockaddr_in fsin; /* the from address of a client */
    int port;                /* service name or port number */
    int msock, ssock;        /* master & slave sockets */
    int alen;                /* from-address length */
    int user_limit = 0;      /* maximum user count */
    switch (argc) {
        case 3:
            user_limit = atoi(argv[2]);
        case 2:
            if ((port = atoi(argv[1])) != 0)
                break;
        default:
            cerr_exit(error_ss << "Usage: socks_server [port] ([user_limit])");
    }
    /* do not wait for child process */
    signal(SIGCHLD, child_exit);
    child_count = 0;
    /* listen on port */
    msock = server_sock(port, 30);
    while (true) {
        /* connected from client */
        alen = sizeof(fsin);
        ssock = accept(msock, (struct sockaddr *)&fsin, (socklen_t *)&alen);
        if (ssock < 0) {
            cerr_exit(error_ss << "Accept failed: " << strerror(errno));
        }
        /* fork for slave */
        int pid;
        while ((user_limit != 0 && child_count >= user_limit) || (pid = fork()) < 0) {
            usleep(100);
        }
        child_count++;
        if (pid == 0) {
            /* Child process */
            close(msock);
            socks4_handler(ssock, fsin);
            exit(0);
        } else {
            /* Parent process */
            close(ssock);
        }
    }
    return 0;
}
